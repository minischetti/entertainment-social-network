import { Component, OnInit } from '@angular/core';
import *  as algoliasearch from 'algoliasearch';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  constructor() { }

  searchClient = algoliasearch('2NXIA6O6TN', '8a7ad007b9d4562987448c1a5ac722a3');
  searchIndex = this.searchClient.initIndex('users');

  searchQuery: string;
  searchResults;
  searchResultsVisible: boolean;

  ngOnInit() {}

  search(query: string) {
    if (query && query.trim() !== "") {
      if (query === this.searchQuery) {
        this.searchResultsVisible = true;
        return;
      }

      this.searchQuery = query;
      this.clearSearchResults();

      this.searchIndex.search({ query: query }, (error, content) => {
        if (error) throw error;

        console.log(`Your search for "${query}" returned ${content.hits.length} results. They are:`, content.hits);

        this.searchResults = content.hits;
        this.searchResultsVisible = true;
      });
    }
  }

  hideSearchResults() {
    console.log("Blur!");
    this.searchResultsVisible = false;
  }

  clearSearchResults() {
    this.searchResults = [];
  }

}
