import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { PostService } from '../post.service';
import { Topic } from '../data-model';
import { SearchService } from '../search.service';
import { ImageService } from '../image.service';

@Component({
    selector: 'app-post-box',
    templateUrl: './post-box.component.html',
    styleUrls: ['./post-box.component.css']
})

export class PostBoxComponent implements OnInit {

    constructor(private postService: PostService,
        private searchService: SearchService,
        private imageService: ImageService,
        private formBuilder: FormBuilder) { }

    @Input() currentUserProfile;

    visible: boolean;
    selectedTopic: any;
    results: Topic[];

    reviewsFromFollowed;

    newPost = this.formBuilder.group({
        author: [''],
        text: [''],
        rating: [true],
        topic: [''],
        date: ['']
    });

    ngOnInit() {
        this.getResults();
    }

    setPostBoxVisibility(visible: boolean) {
        this.visible = visible;
    }

    addPost(postForm) {
        this.newPost.patchValue({
            author: this.currentUserProfile.userIdentifier,
            topic: {
                id: this.selectedTopic.id,
                media_type: this.selectedTopic.media_type
            },
            date: new Date()
        });
        this.postService.addPost(this.newPost.value)
            .then(result => {
                if (result) {
                    console.log(result);
                    this.resetPostBox(postForm);
                }
            });
    }

    resetPostBox(postForm) {
        this.selectedTopic = null;
        this.results = null;
        this.visible = false;
        postForm.reset();
    }

    search(query: string): void {
        this.searchService.fetchMovieOrShow(query);
    }

    constructImageUrl(imagePath: string, size: string) {
        return this.imageService.constructImageUrl(imagePath, size);
    }

    constructPosterImageUrl(imagePath: string) {
        return this.imageService.constructPosterImageUrl(imagePath);
    }

    getResults(): void {
        this.searchService.results
            .subscribe(results => this.results = results);
    }

    setSelectedTopic(topic: Topic) {
        this.selectedTopic = topic;
    }
}
