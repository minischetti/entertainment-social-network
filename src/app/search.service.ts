import { Injectable } from '@angular/core';
import { Subject, Subscription } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Results } from './data-model';
import { ConfigurationService } from './configuration.service';
import { AngularFirestore } from 'angularfire2/firestore';

@Injectable({
    providedIn: 'root'
})
export class SearchService {

    constructor(private httpClient: HttpClient, private infoService: ConfigurationService, private database: AngularFirestore) { }

    results: any = new Subject<any>();
    apiKey = this.infoService.apiKey;

    fetchMovieOrShow(query: string): Subscription {
        const requestUrl = `https://api.themoviedb.org/3/search/multi?api_key=${this.apiKey}&page=1&include_adult=false&query=${query}`;
        return this.httpClient.get<Results>(requestUrl)
            .subscribe(data => {
                this.setResults(data.results);
                console.log(`Search Results for "${query}": `, data.results);
            });
    }

    fetchTopicById(type: string, id: string) {
        let requestUrl;
        if (type === 'tv') {
            requestUrl = `https://api.themoviedb.org/3/tv/${id}?api_key=${this.apiKey}&language=en-US`;
        } else if (type === 'movie') {
            requestUrl = `https://api.themoviedb.org/3/movie/${id}?api_key=${this.apiKey}&language=en-US`;
        }
        return this.httpClient.get<Results>(requestUrl);
    }

    setResults(results): void {
        this.results.next(results);
    }
}
