import { Component, OnInit, Input } from '@angular/core';
import {ProfileService} from '../profile.service';

@Component({
  selector: 'app-user-panel',
  templateUrl: './user-panel.component.html',
  styleUrls: ['./user-panel.component.css']
})
export class UserPanelComponent {

  constructor() { }

  @Input() currentUserProfile;
}
