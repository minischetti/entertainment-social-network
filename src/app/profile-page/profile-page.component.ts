import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ProfileService } from '../profile.service';
import { UserProfile } from '../data-model';
import { Subscription } from 'rxjs';

@Component({
    selector: 'app-profile-page',
    templateUrl: './profile-page.component.html',
    styleUrls: ['./profile-page.component.css']
})
export class ProfilePageComponent implements OnInit, OnDestroy {

    constructor(private route: ActivatedRoute, private router: Router, private profileService: ProfileService) {
        this.router.routeReuseStrategy.shouldReuseRoute = () => false;
    }

    currentUserProfileSubscription: Subscription;
    userProfileSubscription: Subscription;

    currentUserProfile: UserProfile;
    userProfile: UserProfile;

    // posts: DocumentData[];
    // profile: DocumentData[];

    ngOnInit() {
        const userIdentifier = this.route.snapshot.paramMap.get('userIdentifier');
        this.currentUserProfileSubscription = this.profileService.currentUserProfile.subscribe((currentUserProfile: UserProfile) => {
            if (currentUserProfile) {
                this.currentUserProfile = currentUserProfile;
            }
        })
        this.userProfileSubscription = this.profileService.getProfileByUserIdentifier(userIdentifier).subscribe((userProfile: UserProfile) => {
            if (userProfile) {
                this.userProfile = userProfile;
            } else {
                this.router.navigate(['not-found']);
            }
        });
    }

    ngOnDestroy() {
        const subscriptions = [this.userProfileSubscription, this.currentUserProfileSubscription];
        subscriptions.forEach(subscription => {
            if (subscription) {
                subscription.unsubscribe();
            }
        })
    }

    // getProfile() {
    //   this.profileService.currentUser
    //     .subscribe(user => this.profile = user);
    // }
    //
    // getPosts() {
    //   // this.profileService.posts
    //   //   .subscribe(posts => this.posts = posts);
    //   this.profileService.posts
    //     .subscribe(posts => {
    //       this.posts = posts;
    //     });
    // }

}
