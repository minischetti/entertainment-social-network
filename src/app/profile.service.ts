import { Injectable } from '@angular/core';
import { AngularFirestore } from 'angularfire2/firestore';
import { ReplaySubject } from 'rxjs';
import { AngularFireAuth } from 'angularfire2/auth';
import { Credentials, NewAccount, UserProfile } from './data-model';
import { Router } from '@angular/router';

@Injectable({
    providedIn: 'root'
})
export class ProfileService {

    userIdentifier: string;
    currentUserProfile = new ReplaySubject<UserProfile>(1);

    constructor(private database: AngularFirestore, private authClient: AngularFireAuth, private router: Router) {
        this.authClient.auth.onAuthStateChanged(user => {
            if (user) {
                this.userIdentifier = user.uid;
                this.getCurrentUserProfile();
            } else {
                this.resetApp();
            }
        });
    }

    createUserAuthenticationAccount(newAccountCredentials: NewAccount): Promise<boolean> {
        return this.authClient.auth.createUserWithEmailAndPassword(newAccountCredentials.email, newAccountCredentials.password)
            .then((result) => {
                this.createUserProfileInDatabase(newAccountCredentials, result);
                console.log('Successfully created an account.');
                return true;
            })
            .catch(error => {
                console.error(error.message);
                console.error('There was an error creating an account.');
                return false;
            });
    }

    createUserProfileInDatabase(newAccountCredentials: NewAccount, result) {
        this.database.collection('users').doc(result.user.uid).set({
            userName: newAccountCredentials.userName,
            firstName: newAccountCredentials.firstName,
            lastName: newAccountCredentials.lastName,
            following: [result.user.uid],
            userIdentifier: result.user.uid,
            email: newAccountCredentials.email
        })
            .then(() => {
                this.router.navigate(['']);
                console.log('Successfully created profile in database.');
            })
            .catch(() => {
                console.error('There was an error creating this profile in the database.');
            });
    }

    login(credentials: Credentials): Promise<boolean> {
        return this.authClient.auth.signInWithEmailAndPassword(credentials.email, credentials.password)
            .then(() => {
                console.log('You\'ve successfully logged in.');
                this.router.navigate(['']);
                return true;
            })
            .catch(error => {
                console.error('There was an error logging you in.');
                switch (error.code) {
                    default:
                        alert(error.message);
                        break;
                }
                return false;
            });
    }

    /**
     * Updates an existing post with new content.
     * FUTURE - Include the option to change your rating.
     * @param postId the ID of the post we intend to update
     * @param updatedPost the updated review content
     */
    updateAccount(accountChanges): Promise<void> {
        const updatedAccount: any = {}

        if (accountChanges.firstName) {
            updatedAccount.firstName = accountChanges.firstName;
        }
        if (accountChanges.lastName) {
            updatedAccount.lastName = accountChanges.lastName;
        }
        if (accountChanges.userName) {
            updatedAccount.userName = accountChanges.userName;
        }

        return this.database.collection('users').doc(this.userIdentifier).update(updatedAccount);
    }

    logout() {
        this.authClient.auth.signOut().then(() => {
            console.log('You\'ve successfully logged out.');
            this.router.navigate(['/login']);
        }).catch(() => {
            console.log('There was an error logging you out.');
        });
    }

    getCurrentUserProfile() {
        this.database.collection('users').doc(this.userIdentifier).valueChanges().subscribe((userProfile: UserProfile) => {
            this.currentUserProfile.next(userProfile);
        });
    }

    getProfileByUserIdentifier(userIdentifier: string) {
        return this.database.collection('users').doc(userIdentifier).valueChanges();
    }

    isAuthenticated(): any {
        return this.authClient.authState;
    }

    resetApp() {
        console.log('Resetting app...');
        this.userIdentifier = null;
        this.currentUserProfile.next(null);
    }
}
