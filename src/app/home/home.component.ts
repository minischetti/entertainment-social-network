import { Component, OnInit } from '@angular/core';
import { ProfileService } from '../profile.service';
import { UserProfile }  from '../data-model';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

    constructor(private profileService: ProfileService) { }

    currentUserProfile: UserProfile;

    ngOnInit() {
        this.profileService.currentUserProfile.subscribe(currentUserProfile => {
            if (currentUserProfile) {
                this.currentUserProfile = currentUserProfile;
            } else {
                this.currentUserProfile = null;
            }
        });
    }

}
