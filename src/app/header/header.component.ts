import { Component, OnInit, Input } from '@angular/core';
import { ProfileService } from '../profile.service';
import { UserProfile } from '../data-model';
import { Router } from '@angular/router';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: [ './header.component.css']
})

export class HeaderComponent implements OnInit {

    @Input() currentUserProfile : UserProfile;

    constructor(private profileService: ProfileService, private router: Router) { }

    ngOnInit() { }

    goHome() {
        this.router.navigate(['']);
    }

    goToSettings() {
        this.router.navigate(['settings']);
    }

    logout() {
        this.profileService.logout();
    }
}
