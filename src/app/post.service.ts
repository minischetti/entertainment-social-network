import { Injectable } from '@angular/core';
import {Post, Topic} from './data-model';
import {Subject} from 'rxjs';
import {ProfileService} from './profile.service';
import {AngularFirestore, AngularFirestoreCollection} from 'angularfire2/firestore';

@Injectable({
    providedIn: 'root'
})
export class PostService {

    constructor(private profileService: ProfileService, private db: AngularFirestore) {
        this.profileService.currentUserProfile.subscribe(currentUserProfile => {
            if (currentUserProfile) {
                this.userIdentifier = currentUserProfile.userIdentifier;
                this.getPostsCollection();
            }
        });
    }

    posts: AngularFirestoreCollection<any>;
    userIdentifier: string;
    selectedTopic = new Subject<Topic>();

    /**
     * Sets the selected topic.
     * @param topic
     */
    setSelectedTopic(topic: Topic): void {
        console.log(`You've selected ${topic.title || topic.name}.`);
        this.selectedTopic.next(topic);
    }

    /**
     * Gets the current user's posts.
     */
    getPostsCollection() {
        this.posts = this.db.collection('users').doc(this.userIdentifier).collection('posts');
    }

    /**
     * Checks if the current user has already made a post about a particular topic.
     * @param topicId the ID of the topic we intend to check against
     */
    checkIfPostOnTopicExists(topicId) {
        return this.db.collection('users').doc(this.userIdentifier).collection('posts').ref.where('topic.id', '==', topicId)
            .get()
            .then(querySnapshot => {
                if (querySnapshot.docs.length) {
                    console.log('A post already exists for this topic.');
                    return true;
                } else {
                    return false;
                }
            });
    }

    /**
     * Checks if a post already exists on a topic, and if not, proceeds to create the post.
     * @param post an object containing information regarding the author, the review, the topic and the date
     */
    addPost(post: Post) {
        return this.checkIfPostOnTopicExists(post.topic.id)
            .then(doesPostExist => {
                if (!doesPostExist) {
                    console.log(post);
                    this.createPost(post);
                    return true;
                }
            });
    }

    /**
     * Creates a post with the given review information.
     * @param post an object containing information regarding the author, the review, the topic and the date
     */
    createPost(post: Post): void {
        console.log(post);
        this.posts.add({author: post.author, text: post.text, rating: post.rating, topic: post.topic, date: post.date})
            .then(() => {
                console.log(`New post about ${post.topic.title || post.topic.name} created at ${post.date}.`);
                this.selectedTopic.next(null);
            })
            .catch(() => {
                console.log('Uh oh... we had a problem creating your post.');
            });
    }

    // /**
    //  * Updates an existing post with new content.
    //  * FUTURE - Include the option to change your rating.
    //  * @param postId the ID of the post we intend to update
    //  * @param updatedPost the updated review content
    //  */
    // updatePost(postId, updatedPost): void {
    //     this.db.collection('users').doc(this.userIdentifier).collection('posts').doc(postId).update({
    //         text: updatedPost.text
    //     })
    //         .then(() => {
    //             console.log(`Post with ID ${postId} successfully updated.`);
    //         })
    //         .catch(() => {
    //             console.log('Unable to update post.');
    //         });
    // }

    /**
     * Deletes an existing post.
     * @param postId the ID of the post we intend to delete
     */
    deletePost(postId): void {
        this.db.collection('users').doc(this.userIdentifier).collection('posts').doc(postId).delete()
            .then(() => {
                console.log(`Post with ID ${postId} successfully deleted.`);
            })
            .catch(() => {
                console.log('Unable to delete post.');
            });
    }
}
