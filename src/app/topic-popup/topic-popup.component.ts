import { Component, OnInit } from '@angular/core';
import {FeedService} from '../feed.service';
import {ImageService} from '../image.service';

@Component({
    selector: 'app-topic-popup',
    templateUrl: './topic-popup.component.html',
    styleUrls: ['./topic-popup.component.css']
})
export class TopicPopupComponent implements OnInit {

    constructor(private feedService: FeedService,
                private imageService: ImageService) { }

    expandedTopic;
    
    postsFromCurrentUser;
    postsFromFollowedUsers;

    ngOnInit() {
        this.feedService.expandedTopic.subscribe(topic => {
            if (topic) {
                this.expandedTopic = topic;
                this.viewTopicPopup();
            }
        });

        this.feedService.postsFromCurrentUserAndFollowedUsersAboutTopic.subscribe(posts => {
            if (posts) {
                this.postsFromCurrentUser = posts.filter(post => post.currentUser);
                this.postsFromFollowedUsers = posts.filter(post => !post.currentUser);
            }
        });
    }

    setExpandedTopic(topic) {
        this.expandedTopic = topic;
    }

    // POST EXTRAS
    viewTopicPopup() {
        this.postsFromCurrentUser = null;
        this.postsFromFollowedUsers = null;
        this.feedService.getPostsFromCurrentUserAndFollowedUsersAboutTopic(this.expandedTopic.id);
    }

    constructImageUrl(imagePath: string, size: string) {
        return this.imageService.constructImageUrl(imagePath, size);
    }

    constructPosterImageUrl(imagePath: string) {
        return this.imageService.constructPosterImageUrl(imagePath);
    }
}
