import { Component, OnInit, OnDestroy } from '@angular/core';
import { ProfileService } from '../profile.service';
import { UserProfile } from '../data-model';
import { Subscription } from 'rxjs';
import { FormBuilder } from '@angular/forms';

@Component({
    selector: 'app-settings',
    templateUrl: './settings.component.html',
    styleUrls: ['./settings.component.css']
})
export class SettingsComponent implements OnInit, OnDestroy {

    constructor(private profileService: ProfileService, private formBuilder: FormBuilder) { }

    currentUserProfileSubscription: Subscription;
    currentUserProfile: UserProfile;

    settings = this.formBuilder.group({
        firstName: [''],
        lastName: [''],
        userName: [''],
        password: ['']
    });

    ngOnInit() {
        this.currentUserProfileSubscription = this.profileService.currentUserProfile.subscribe(currentUserProfile => {
            if (currentUserProfile) {
                this.currentUserProfile = currentUserProfile;
            }
        });
    }

    ngOnDestroy() {
        this.currentUserProfileSubscription.unsubscribe();
        this.currentUserProfile = null;
    }

    updateAccount(form) {
        this.profileService.updateAccount(this.settings.value)
            .then(() => {
                console.log(`Account updated successfully.`);
                form.reset();
            })
            .catch(() => {
                console.log('Unable to update account.');
            });;
    }
}
