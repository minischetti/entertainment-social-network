import { Component } from '@angular/core';
import { ProfileService } from './profile.service';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})


export class AppComponent {
    currentUserProfile;

    constructor(private profileService: ProfileService) {
        this.profileService.currentUserProfile.subscribe(currentUserProfile => {
            this.currentUserProfile = currentUserProfile;
        });
    }
}
