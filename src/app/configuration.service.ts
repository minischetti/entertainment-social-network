import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ConfigurationService {

  constructor(private httpClient: HttpClient) {
    this.getConfiguration();
  }

  configuration: any = new Subject();
  apiKey = 'a99bc4d5a83ca42b26b0f05df051a79e';

  getConfiguration() {
    const requestUrl = `https://api.themoviedb.org/3/configuration?api_key=${this.apiKey}`;
    return this.httpClient.get(requestUrl)
      .subscribe(data => {
        this.setConfiguration(data);
        console.log('Configuration: ', data);
      });
  }

  setConfiguration(configuration): void {
    this.configuration.next(configuration);
  }
}
