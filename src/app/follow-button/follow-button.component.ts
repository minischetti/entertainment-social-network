import { Component, Input, OnInit } from '@angular/core';
import { AngularFirestore } from 'angularfire2/firestore';
import * as firebase from 'firebase';
import FieldValue = firebase.firestore.FieldValue;
import { FirestoreArrayOperations, UserProfile } from '../data-model';

@Component({
    selector: 'app-follow-button',
    templateUrl: './follow-button.component.html',
    styleUrls: ['./follow-button.component.css']
})
export class FollowButtonComponent implements OnInit {

    @Input() otherUserProfile: UserProfile;

    @Input() currentUserProfile: UserProfile;

    alreadyFollowing: boolean;

    constructor(private database: AngularFirestore) { }

    ngOnInit() {
        this.alreadyFollowing = this.currentUserProfile.following.includes(this.otherUserProfile.userIdentifier);
    }

    toggleFollowStatus() {
        let operation;

        if (this.alreadyFollowing) {
            operation = FirestoreArrayOperations.REMOVE;
        } else {
            operation = FirestoreArrayOperations.ADD;
        }

        this.updateFollowing(this.currentUserProfile.userIdentifier, 'following', this.otherUserProfile.userIdentifier, operation);
        this.updateFollowers(this.otherUserProfile.userIdentifier, 'followers', this.currentUserProfile.userIdentifier, operation);
    }

    updateFollowers(affectedUser, arrayName, arrayField, operation) {
        if (operation === FirestoreArrayOperations.ADD) {
            this.database.collection('users').doc(affectedUser).update(
                {
                    'followers':
                        FieldValue.
                            arrayUnion(arrayField)
                });
        } else {
            this.database.collection('users').doc(affectedUser).update(
                {
                    'followers':
                        FieldValue.
                            arrayRemove(arrayField)
                });
        }
    }

    updateFollowing(affectedUser, arrayName, arrayField, operation) {
        if (operation === FirestoreArrayOperations.ADD) {
            this.database.collection('users').doc(affectedUser).update(
                {
                    'following':
                        FieldValue.
                            arrayUnion(arrayField)
                }).then(() => this.alreadyFollowing = true);

        } else {
            this.database.collection('users').doc(affectedUser).update(
                {
                    'following':
                        FieldValue.
                            arrayRemove(arrayField)
                }).then(() => this.alreadyFollowing = false);
        }
    }
}
