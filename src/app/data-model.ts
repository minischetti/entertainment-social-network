export class Post {
    constructor(
        public author: string,
        public text: string,
        public rating: boolean,
        public topic: any,
        public date: Date
    ) { }
}

export enum FirestoreArrayOperations {
    ADD = 'arrayUnion',
    REMOVE = 'arrayRemove'
}

export class UpdatedPost {
    constructor(
        public text: string
    ) { }
}

export interface Credentials {
    email: string,
    password: string
}

export interface NewAccount {
    firstName: string,
    lastName: string,
    userName: string,
    email: string,
    password: string
}

export interface UserPopup {
    currentUserProfile: UserProfile,
    otherUserProfile: UserProfile,
    xPos: number,
    yPos: number
}

// export class Credentials {
//   constructor(
//     public email: string,
//     public password: string
//   ) {}
// }

// export class NewAccountCredentials {
//     constructor(
//         public firstName: string,
//         public lastName: string,
//         public displayName: string,
//         public userName: string,
//         public email: string,
//         public password: string
//     ) {}
// }

export interface UserProfile {
    firstName: string,
    lastName: string,
    followers: Array<string>,
    following: Array<string>,
    userIdentifier: string,
    userName: string
}

export class Topic {
    adult: boolean;
    backdrop_path: string;
    genre_ids: Array<number>;
    id: number;
    media_type: string;
    original_language: string;
    original_title?: string;
    original_name?: string;
    overview: string;
    popularity: number;
    poster_path: string;
    release_date: string;
    title?: string;
    name?: string;
    video: boolean;
    vote_average: number;
    vote_count: number;
}

export class Results {
    results: Topic[];
}

export class Profile {
    name: Name;
    displayName: string;
    userName: string;
    posts: Topic[];
}

export class Name {
    first: string;
    last: string;
}
