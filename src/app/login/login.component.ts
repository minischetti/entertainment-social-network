import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ProfileService } from '../profile.service';
import { Router } from '@angular/router';
import { Credentials, NewAccount }  from '../data-model';

@Component({
    selector: 'app-home',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

    constructor(private profileService: ProfileService, private formBuilder: FormBuilder, private router: Router) { }

    credentials = this.formBuilder.group({
        email: [''],
        password: ['']
    });

    newAccount = this.formBuilder.group({
        firstName: [''],
        lastName: [''],
        userName: [''],
        email: [''],
        password: ['']
    });

    passwordHidden = true;

    ngOnInit() { }

    resetFormAndNavigateToFeed(form: any) {
        this.router.navigate(['']);
        form.reset();
    }

    createNewAccount(form: NewAccount) {
        this.profileService.createUserAuthenticationAccount(this.newAccount.value).then(() => this.resetFormAndNavigateToFeed(form));
    }

    login(form: Credentials) {
        this.profileService.login(this.credentials.value).then(() => this.resetFormAndNavigateToFeed(form));
    }

    toggleFormType(form, event) {
        event.preventDefault();
        switch(form.type) {
            case "password":
                form.type = "text";
                this.passwordHidden = true;
                break;
                case "text":
                form.type = "password";
                this.passwordHidden = false;
                break;
        }
    }

}
