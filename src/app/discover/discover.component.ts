import { Component, OnInit, Input } from '@angular/core';
import {AngularFirestore} from 'angularfire2/firestore';

@Component({
    selector: 'app-discover',
    templateUrl: './discover.component.html',
    styleUrls: ['./discover.component.css']
})
export class DiscoverComponent implements OnInit {

    users;

    @Input() currentUser;

    constructor(private database: AngularFirestore) { }

    ngOnInit() {
        this.database.collection('users').ref
        // .limit(10)
            .get()
            .then(users => {
                let userProfiles = [];
                users.forEach(user => {
                    const currentUser = user.data().userIdentifier === this.currentUser.userIdentifier;

                    if (user && !currentUser) {
                        userProfiles = [user.data(), ...userProfiles];
                    }
                });
                this.users = userProfiles;
            });
    }
}
