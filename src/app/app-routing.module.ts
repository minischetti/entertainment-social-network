import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { ProfilePageComponent } from './profile-page/profile-page.component';
import { FeedComponent } from './feed/feed.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from "./login/login.component";
import { AuthGuard } from './auth/auth.guard';
import { SettingsComponent } from './settings/settings.component';

const appRoutes: Routes = [
    // { path: '', redirectTo: '/home', pathMatch: 'full' },
    {
        path: '',
        pathMatch: 'full',
        component: HomeComponent,
        canActivate: [AuthGuard]
    },
    { path: 'login', component: LoginComponent },
    { path: 'settings', component: SettingsComponent },
    { path: ':userIdentifier', component: ProfilePageComponent },
    { path: 'not-found', component: PageNotFoundComponent },
    // { path: '**', pathMatch: 'full', redirectTo: '/not-found' },
];

@NgModule({
    imports: [
        RouterModule.forRoot(
            appRoutes,
            // { enableTracing: true } // <-- debugging purposes only
        )
    ],
    exports: [
        RouterModule
    ]
})
export class AppRoutingModule { }
