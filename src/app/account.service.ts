import { Injectable } from '@angular/core';
import {AngularFireAuth} from 'angularfire2/auth';
import {Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AccountService {

  constructor(public afAuth: AngularFireAuth) {
    this.getCurrentUser();
  }

  userIdentifier = new Subject<string>();
  isAuthenticated = new Subject<boolean>();

  createUser(credentials) {
    this.afAuth.auth.createUserWithEmailAndPassword(credentials.email, credentials.password)
      .then(() => {
        console.log('You\'ve successfully created an account.');
      })
      .catch(error => {
        alert(error.message);
        console.log('There was an error creating your account.');
      });
  }

  login(credentials) {
    this.afAuth.auth.signInWithEmailAndPassword(credentials.email, credentials.password)
      .then(() => {
        console.log('You\'ve successfully logged in.');
      })
      .catch(error => {
        const noUserCode = 'auth/currentUser-not-found';
        switch (error.code) {
          case noUserCode:
            this.createUser(credentials);
            break;
          default:
            alert(error.message);
            break;
        }
      });
  }

  logout() {
    this.afAuth.auth.signOut().then(() => {
      console.log('You\'ve successfully logged out.');
    }).catch(() => {
      console.log('There was an error logging you out.');
    });
  }

  // getUser() {
  //   this.userIdentifier
  //     .subscribe(uid => {
  //       this.currentUser = this.db.collection('users').doc(uid);
  //       this.getProfile();
  //       this.getPosts();
  //       this.getFollowedUsers();
  //     }), console.log('There was an error subscribing to userIdentifier.');
  // }

  setUserIdentifier(userIdentifier: string) {
    this.userIdentifier.next(userIdentifier);
  }

  setAuthenticationState(state: boolean) {
    this.isAuthenticated.next(state);
  }

  getCurrentUser() {
    this.afAuth.auth.onAuthStateChanged((user) => {
      if (user) {
        // this.getUser();
        this.setUserIdentifier(user.uid);
        this.setAuthenticationState(true);
        console.log('User is signed in.');
      } else {
        this.setUserIdentifier(null);
        this.setAuthenticationState(false);
        console.log('No currentUser is signed in.');
      }
    });
  }
}
