import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { ProfileService } from '../profile.service';
import { FeedService } from '../feed.service';
import { SearchService } from '../search.service';
import * as moment from 'moment';
import { ImageService } from '../image.service';
import { PostService } from '../post.service';
import { UserProfile, UserPopup } from '../data-model';
import { Subscription, combineLatest } from 'rxjs';
import { AngularFirestore } from 'angularfire2/firestore';

@Component({
    selector: 'app-feed',
    templateUrl: './feed.component.html',
    styleUrls: ['./feed.component.css']
})
export class FeedComponent implements OnInit, OnDestroy {

    constructor(private profileService: ProfileService,
        private feedService: FeedService,
        private searchService: SearchService,
        private imageService: ImageService,
        private postService: PostService,
        private database: AngularFirestore) { }

    @Input() userProfile: UserProfile;
    @Input() displayPostsFromFollowedUsers: boolean;
    currentUserProfile: UserProfile;
    isFollowingAtLeastOneUser: boolean;
    userPopupPayload: UserPopup;

    // postToEdit: string;
    // updatedPost = new UpdatedPost(null);

    loading: boolean = true;
    feedBelongsToCurrentUser: boolean;

    currentUserProfileSubscription: Subscription;
    postsSubscription: Subscription;
    getProfileForPostSubscription: Subscription;
    fetchTopicByIdSubscription: Subscription;
    posts;



    ngOnInit() {
        this.getCurrentUserProfile();
        this.displayPostsFromFollowedUsers ? this.getPostsFromFollowedUsers() : this.getPostsFromSpecificUser();
    }

    ngOnDestroy() {
        const subscriptions = [this.currentUserProfileSubscription, this.postsSubscription, this.getProfileForPostSubscription, this.fetchTopicByIdSubscription];
        subscriptions.forEach(subscription => {
            if (subscription) {
                subscription.unsubscribe();
            }
        })
    }

    getCurrentUserProfile() {
        this.currentUserProfileSubscription = this.profileService.currentUserProfile.subscribe(currentUserProfile => {
            if (currentUserProfile) {
                this.currentUserProfile = currentUserProfile;
                this.feedBelongsToCurrentUser = this.userProfile.userIdentifier === currentUserProfile.userIdentifier;
                console.log(this.feedBelongsToCurrentUser);
            } else {
                this.currentUserProfile = null;
            }
        });
    }

    getPostsFromSpecificUser(): void {
        this.database.collection('users').doc(this.userProfile.userIdentifier).collection<any>('posts').snapshotChanges().subscribe(posts => {
            if (posts.length) {
                this.posts = this.sortPosts(posts.map(post => this.populatePostWithData(post.payload.doc.data())));
            }
            this.loading = false;
        });
    }

    getPostsFromFollowedUsers(): void {
        if (this.userProfile.following && this.userProfile.following.length) {
            let userObservables: any = [];
            this.userProfile.following.forEach(followedUser => {
                userObservables = [this.database.collection('users').doc(followedUser).collection<any>('posts').snapshotChanges(), ...userObservables];
            });
            this.combinePostsFromMultipleUsers(userObservables);
        }
    }

    /**
     * Combines the observables of multiple users into a single observable.
     * @param userObservables the observables to combine
     */
    combinePostsFromMultipleUsers(userObservables): void {
        combineLatest(userObservables).subscribe((posts: any) => {
            const flattenedPosts = posts.flat(1);

            if (flattenedPosts.length) {
                this.posts = this.sortPosts(flattenedPosts.map(post => this.populatePostWithData(post.payload.doc.data())));
            };

            this.loading = false;
        });
    }

    /**
     * Sorts posts in reverse-chronological order.
     * @param posts
     * @returns {any}
     */
    sortPosts(posts) {
        posts.sort(function (a, b) {
            a = moment(a.date.toMillis());
            b = moment(b.date.toMillis());
            return b - a;
        });
        return posts;
    }

    /**
     * Calculates the time from when a post was created.
     * @param postDate
     * @returns {string}
     */
    calculateTimeFromPostDate(postDate) {
        postDate = postDate.toMillis();
        const timeSincePost = moment(postDate).fromNow();
        return timeSincePost;
    }

    /**
     * Populates a post with the poster's profile as well as topic data.
     * @param post
     * @param userIdentifier
     * @returns {any}
     */
    populatePostWithData(post) {
        this.getProfileForPostSubscription = this.feedService.getProfileForPost(post.author).subscribe(profile => {
            if (profile) {
                post.profile = profile;
            }
        });

        this.fetchTopicByIdSubscription = this.searchService.fetchTopicById(post.topic.media_type, post.topic.id).subscribe(topic => {
            const mediaType = post.topic.media_type;

            if (topic) {
                post.topic = topic;
                post.topic.media_type = mediaType;
            }
        });

        post.currentUser = post.author === this.currentUserProfile.userIdentifier;

        post.timeFromPost = this.calculateTimeFromPostDate(post.date);

        return post;
    }

    /**
     * Calls the image service to construct a poster image URL.
     * @param {string} imagePath
     * @returns {string}
     */
    constructPosterImageUrl(imagePath: string) {
        return this.imageService.constructPosterImageUrl(imagePath);
    }

    // POST FUNCTIONALITY

    /**
     * Calls post service to delete a post.
     * @param postId
     */
    deletePost(postId) {
        this.postService.deletePost(postId);
    }

    // /**
    //  * Toggles edit functionality for the given post ID.
    //  * @param postId
    //  */
    // editPost(postId) {
    //     if (this.postToEdit === postId) {
    //         this.postToEdit = null;
    //     } else {
    //         this.postToEdit = postId;
    //     }
    // }

    // /**
    //  * Determines if a post is editable.
    //  * @param postId
    //  * @returns {boolean}
    //  */
    // postIsEditable(postId): boolean {
    //     return this.postToEdit === postId;
    // }

    // /**
    //  * Calls the post service to update a post, then reset the update form.
    //  * @param postId
    //  * @param postForm
    //  */
    // updatePost(postId, postForm) {
    //     this.postService.updatePost(postId, this.updatedPost);
    //     this.postToEdit = null;
    //     postForm.reset();
    // }

    /**
     * Calls feed service to set the currently expanded topic.
     * @param {string} topicId
     */
    setExpandedTopic(topicId: string) {
        this.feedService.setExpandedTopic(topicId);
    }

    setUserPopupPayload(otherUserProfile: UserProfile, event) {
        const parentElement = event.target;
        this.userPopupPayload = {
            currentUserProfile: this.currentUserProfile,
            otherUserProfile: otherUserProfile,
            xPos: parentElement.offsetLeft + (parentElement.offsetWidth / 2),
            yPos: parentElement.offsetTop + parentElement.offsetHeight
        };
    }

    resetUserPopupPayload() {
        this.userPopupPayload = null;
    }
}
