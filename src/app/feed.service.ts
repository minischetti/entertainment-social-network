import { Injectable } from '@angular/core';
import { ProfileService } from './profile.service';
import { AngularFirestore } from 'angularfire2/firestore';
import { combineLatest, Subject } from 'rxjs';
import { UserProfile } from './data-model';

@Injectable({
    providedIn: 'root'
})
export class FeedService {

    constructor(private profileService: ProfileService, private db: AngularFirestore) {
        this.profileService.currentUserProfile.subscribe(currentUser => {
            if (currentUser) {
                this.currentUser = currentUser;
                // this.getPostsFromFollowedUsers();
            } else {
                this.currentUser = null;
                this.setPostsFromFollowedUsers(null);
            }
        });
    }

    currentUser: UserProfile;

    postsFromFollowedUsers = new Subject<any>();
    postsFromCurrentUserAndFollowedUsersAboutTopic = new Subject<any>();

    expandedTopic = new Subject();

    // /**
    //  * Gets posts from followed users.
    //  * @param followedUsers
    //  */
    // getPostsFromFollowedUsers(): void {
    //     if (this.currentUser.following && this.currentUser.following.length) {
    //         let userObservables: any = [];
    //         this.currentUser.following.forEach(followedUser => {
    //             userObservables = [this.db.collection('users').doc(followedUser).collection<any>('posts').snapshotChanges(), ...userObservables];
    //         });
    //         this.combinePostsFromMultipleUsers(userObservables);
    //     } else {
    //         this.setPostsFromFollowedUsers(null);
    //     }
    // }

    // /**
    //  * Combines the observables of multiple users into a single observable.
    //  * @param userObservables the observables to combine
    //  */
    // combinePostsFromMultipleUsers(userObservables): void {
    //     combineLatest(userObservables).subscribe(posts => {
    //         posts = [].concat(...posts);
    //         posts = posts.map(post => {
    //             const newPost: any = post;
    //             const data = newPost.payload.doc.data();
    //             const id = newPost.payload.doc.id;
    //             return { id, ...data };
    //         });
    //         this.setPostsFromFollowedUsers(posts);
    //     });
    // }

    /**
     * Sets the topic currently expanded to reveal additional information.
     * @param {string} topic
     */
    setExpandedTopic(topic: string) {
        this.expandedTopic.next(topic);
    }

    /**
     * Sets the posts from followed users.
     * @param posts
     */
    setPostsFromFollowedUsers(posts) {
        this.postsFromFollowedUsers.next(posts);
    }

    /**
     * Sets the posts from followed users about a specific topic.
     * @param posts
     */
    setPostsFromCurrentUserAndFollowedUsersAboutTopic(posts) {
        this.postsFromCurrentUserAndFollowedUsersAboutTopic.next(posts);
    }

    /**
     * Gets the profile for a given user identifier.
     * @param userIdentifier
     * @returns {Observable<any>}
     */
    getProfileForPost(userIdentifier) {
        return this.db.collection('users').doc(userIdentifier).valueChanges();
    }

    /**
     * Gets posts from the current user and the current user's followed users about a specific topic.
     * @param {string} topicId
     */
    getPostsFromCurrentUserAndFollowedUsersAboutTopic(topicId: string) {
        let postsFromCurrentUserAndFollowedUsersAboutTopic = [];
        this.currentUser.following.forEach((userIdentifier) => {
            this.getSpecificPostFromUser(userIdentifier, topicId).then((result) => {
                if (result) {
                    const post: any = {};
                    post.info = result;

                    this.profileService.getProfileByUserIdentifier(userIdentifier).subscribe(profile => {
                        post.profile = profile;

                        post.currentUser = userIdentifier === this.currentUser.userIdentifier;

                        postsFromCurrentUserAndFollowedUsersAboutTopic = [post, ...postsFromCurrentUserAndFollowedUsersAboutTopic];
                        this.setPostsFromCurrentUserAndFollowedUsersAboutTopic(postsFromCurrentUserAndFollowedUsersAboutTopic);
                    });
                }
            });
        });
    }

    /**
     * Gets a specific post from a specific userIdentifier, based on the given userIdentifier identifier and topic ID.
     * @param {string} userIdentifier
     * @param {string} topicId
     * @returns {Promise<firebase.firestore.DocumentData>}
     */
    getSpecificPostFromUser(userIdentifier: string, topicId: string) {
        return this.db.collection('users').doc(userIdentifier).collection('posts').ref.where('topic.id', '==', topicId)
            .get()
            .then(querySnapshot => {
                if (querySnapshot.docs.length) {
                    // console.log(`${userIdentifier} - Yes`);
                    return querySnapshot.docs[0].data();
                }
            });
    }

}
