import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AngularFireModule } from 'angularfire2';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { environment } from '../environments/environment';

import { AppComponent } from './app.component';
import { FeedComponent } from './feed/feed.component';
import { TopicPopupComponent } from './topic-popup/topic-popup.component';
import { ProfilePageComponent } from './profile-page/profile-page.component';
import { PostBoxComponent } from './post-box/post-box.component';
import { PostComponent } from './post/post.component';
import { LoginComponent } from './login/login.component';
import {ProfileService} from './profile.service';
import { UserPanelComponent } from './user-panel/user-panel.component';
import {ConfigurationService} from './configuration.service';
import {ImageService} from './image.service';
import {HomeComponent} from './home/home.component';
import { NotificationComponent } from './notification/notification.component';
import {FeedService} from './feed.service';
import { SearchComponent } from './search/search.component';
import { UserPopupComponent } from './user-popup/user-popup.component';
import { UserListComponent } from './user-list/user-list.component';
import { DiscoverComponent } from './discover/discover.component';
import { FollowButtonComponent } from './follow-button/follow-button.component';
import { AppRoutingModule } from './app-routing.module';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { HeaderComponent } from './header/header.component';
import { MatButtonModule, MatCheckboxModule, MatMenuModule, MatTabsModule, MatInputModule, MatExpansionModule, MatProgressSpinnerModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';
import { SettingsComponent } from './settings/settings.component';

@NgModule({
  declarations: [
    AppComponent,
    FeedComponent,
    TopicPopupComponent,
    ProfilePageComponent,
    PostBoxComponent,
    PostComponent,
    LoginComponent,
    UserPanelComponent,
    HomeComponent,
    NotificationComponent,
    SearchComponent,
    UserPopupComponent,
    UserListComponent,
    DiscoverComponent,
    FollowButtonComponent,
    PageNotFoundComponent,
    HeaderComponent,
    SettingsComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    MatCheckboxModule,
    MatButtonModule,
    MatMenuModule,
    MatTabsModule,
    MatInputModule,
    MatExpansionModule,
    MatProgressSpinnerModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule,
    AngularFireAuthModule,
    AppRoutingModule,
    RouterModule
  ],
  providers: [ProfileService, ConfigurationService, ImageService, FeedService],
  bootstrap: [AppComponent],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ]
})
export class AppModule { }
