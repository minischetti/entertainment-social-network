import { Injectable } from '@angular/core';
import {ConfigurationService} from './configuration.service';

@Injectable({
  providedIn: 'root'
})
export class ImageService {

  constructor(private infoService: ConfigurationService) {
    this.getConfiguration();
  }

  configuration: any;

  getConfiguration(): void {
    this.infoService.configuration
      .subscribe(configuration => this.configuration = configuration);
  }

  constructImageUrl(imagePath: string, size: string) {
    switch (size) {
      case 'orig':
        size = 'original';
        break;
      case 'lg':
        size = '1280';
        break;
      case 'md':
        size = '780';
        break;
      case 'sm':
        size = '300';
        break;
    }
    return `${this.configuration.images.base_url}w${size}${imagePath}`;
  }

  constructPosterImageUrl(imagePath: string) {
      return `${this.configuration.images.base_url}w342${imagePath}`;
  }
}
