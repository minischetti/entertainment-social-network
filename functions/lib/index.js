"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const functions = require("firebase-functions");
const admin = require("firebase-admin");
const algoliasearch = require("algoliasearch");
// import * as firebase from "firebase";
// import { object } from 'firebase-functions/lib/providers/storage';
admin.initializeApp(functions.config().firebase);
const env = functions.config();
const searchClient = algoliasearch(env.algolia.app_id, env.algolia.api_key);
const userSearchIndex = searchClient.initIndex('users');
// const db = admin.firestore();
/**
 * Adds a user to the search index.
 */
exports.addUserToIndex = functions.firestore.document('users/{userId}').onCreate(documentSnapshot => {
    const data = documentSnapshot.data();
    const userProfile = {
        objectID: documentSnapshot.id,
        firstName: data.firstName,
        lastName: data.lastName,
        userName: data.userName
    };
    return userSearchIndex.addObject(userProfile);
});
/**
 * Removes a user from the user search index.
 */
exports.removeUserFromIndex = functions.firestore.document('users/{userId}').onDelete(documentSnapshot => {
    return userSearchIndex.deleteObject(documentSnapshot.id);
});
/**
 * Does a partial update to a user in the user search index if their real or user name has been updated.
 */
exports.updateSearchIndexOnUserUpdate = functions.firestore.document('users/{userId}').onUpdate(change => {
    const oldProfileData = change.before.data();
    const newProfileData = change.after.data();
    const newFirstName = newProfileData.firstName !== oldProfileData.firstName;
    const newLastName = newProfileData.lastName !== oldProfileData.lastName;
    const newUserName = newProfileData.userName !== oldProfileData.userName;
    const profileHasBeenUpdated = newFirstName || newLastName || newUserName;
    console.log("Does profile update include a new first name?", newFirstName);
    console.log("Does profile update include a new last name?", newLastName);
    console.log("Does profile update include a new user name?", newUserName);
    const updatedProfileIndex = {
        objectID: change.after.id
    };
    if (newFirstName) {
        updatedProfileIndex.firstName = newProfileData.firstName;
    }
    if (newLastName) {
        updatedProfileIndex.firstName = newProfileData.lastName;
    }
    if (newUserName) {
        updatedProfileIndex.userName = newProfileData.userName;
    }
    if (profileHasBeenUpdated) {
        return userSearchIndex.partialUpdateObject(updatedProfileIndex);
    }
});
// exports.sendEmailVerification = functions.auth.user().onCreate(user => {
//     const currentUser = firebase.auth().currentUser;
//     currentUser.sendEmailVerification().then(function() {
//         console.log(`Successfully sent verification email to ${currentUser.email}`);
//     }).catch(function(error) {
//         console.error(error);
//     });
// });
// exports.createAccountShell = functions.auth.user().onCreate(user => {
//     const data = {
//         displayName: '',
//         following: [user.uid],
//         name: {
//             first: '',
//             last: ''
//         },
//         userName: ''
//     };
//     db.collection('users').doc(user.uid).set(data)
//         .then(() => {
//             console.log(`Successfully created account shell for UID ${user.uid}.`);
//         })
//         .catch(() => {
//             console.error(`There was an error creating account shell for UID ${user.uid}.`);
//         });
// });
//# sourceMappingURL=index.js.map