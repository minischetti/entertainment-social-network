export interface ProfileIndex {
    objectID: string,
    firstName?: string,
    lastName?: string,
    userName?: string
}